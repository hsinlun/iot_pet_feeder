import RPi.GPIO as GPIO
import time

servoPIN = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN, GPIO.OUT)

p = GPIO.PWM(servoPIN, 50)  # GPIO 17 for PWM with 50Hz
p.start(2.5)  # Initialization
try:
    for i in range(0, 2):
        p.ChangeDutyCycle(12.5)
        time.sleep(0.75)
        p.ChangeDutyCycle(2.5)
        time.sleep(1)
except KeyboardInterrupt:
    p.stop()
finally:
    GPIO.cleanup()

