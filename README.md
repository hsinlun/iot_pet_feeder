# 寵物餵食管家 Pet Feeder 
GitLab顯示有點跑板，可以在 HackMD 上閱讀[此文件](https://hackmd.io/@hsinlun/S1Q6hECCw)

## 關於專案
- 寵物餵食器是許多人實作過的專題。除了能夠自動餵食之外，還希望能透過回傳寵物吃飯的資訊讓主人更加掌握寵物狀況。

## 專案架構
- 此專題可以分成兩大部分：
    1. 定時自動餵食：
        - 此部分透過利用` crontab `間隔12小時執行`feed.py`達成。`feed.py`會旋轉馬達放下飼料擋板倒飼料。而執行一次`feed.py`會轉動兩次馬達，這是因為轉動一次馬達所倒出的飼料有點太少了，兩次比較符合一般小型犬一餐的飼料量。
    2. 偵測寵物吃飯並回傳資訊：
        - 此部分由`final.py`執行。藉由餵食器前方的PIR sensor，感應寵物是否來吃飯。若感應到寵物來吃飯了，便拍照並將照片上傳至imgur圖床，再利用Line Notify將照片與寵物開始吃飯的時間傳給主人。
>※ 為何要先上傳至imgur圖床再傳送照片？
>因為若要使用lineNotify上傳本地圖片，官方api有一小時只能傳一張照片的限制。
>故先上傳至imgur圖床後取得照片連結，再透過其網址傳送照片。
>
>詳見 [Line Notify 官方 api](https://notify-bot.line.me/doc/en/)。

## 專案所需材料
- 1個 Raspberry pi 4
- 1個鏡頭
- 1個 PIR motion sensor
- 1個 Servomotor SG90
- 1組電子秤模組(由下列材料組成)：
    - HX711
    - 5公斤壓力感測器
    - 秤重套件組
- 很多紙箱
- 竹筷數雙
- 橡皮筋數個
- 瓶口較寬的塑膠瓶一個
- 棉線(我用的是縫衣線，有點太細了，測試階段就快斷了QQ 棉線可能更耐用)
- 冰棒棍一包(也可以用竹筷取代，但冰棒棍在組合結構中比較好控制橡皮筋位置)
- 吸管數支(可以不用，但吸管套在竹筷上可以減少和線之間的摩擦，不然線會很容易斷掉)

## 餵食器製作
- 餵食器主體製作大致參考[此影片](https://www.youtube.com/watch?v=4hXE9fyOXUY)。
- 不過，因為馬達扭力無法負荷原影片設計的按壓方式，這邊我改成用馬達拉動線，再透過線拉動飼料擋板。
![](https://i.imgur.com/Q4Bm8K1.png)
- 因此，在製作飼料擋板的時候，要在**下圖飼料擋板的背面**用冰棒棍組合出一個小洞，方便之後穿線進去。
![](https://i.imgur.com/j6DvUVO.png)
- 如下圖，穿好洞之後組合進飼料機的話背面會像這樣
![](https://i.imgur.com/44jR9Zb.jpg)
- 除了穿線之外，飼料機背面也要打個洞，然後在適當的高度黏上竹筷(竹筷可以用吸管套住以降低摩擦)。
- 記得之後棉線要從竹筷下面往上拉喔。
![](https://i.imgur.com/4Ckzelb.gif)

> ※ 注意餵食器出飼料口的高度要比影片裡的更高！
> 因為除了飼料碗還需要保留電子秤的高度


## 電路設計
![](https://i.imgur.com/inw0KBx.png)
### 說明
- 紅色均為正極，在此由樹莓派的 `實體2` 連接到麵包板以便其他元件連接
- 灰色均為接地，在此由樹莓派的 `實體39` 連接到麵包板以便其他元件連接
- 以下將忽略正極與接地的說明
- PIR sensor
![](https://i.imgur.com/Dp9TAKp.png)
    - 黃色接腳為訊號，在此我將它連接到 `GPIO18(實體 12)`
- LED (RED)
![](https://i.imgur.com/xFpkuU3.png)
    - 用於調整 PIR 靈敏度與角度
    - 在此我將它連接到 `GPIO23(實體16)`
- HX711
![](https://i.imgur.com/t2Mqi5a.png)
    - HX711有紅板與綠板之分，而電路圖圖示是紅板的。因此附上我所使用的綠板接腳配置
    ![](https://i.stack.imgur.com/OYfAo.jpg =50%x)
    - 褐色為DT，在此我將它連接到 `GPIO6(實體31)`
    - 黑色為SCK，在此我將它連接到 `GPIO5(實體29)`
- SG90
![](https://i.imgur.com/xTsELOT.png)
    - 黃色接腳為訊號，在此我將它連接到 `GPIO17(實體11)`

## 程式執行注意事項
- 務必先執行`initial_swap.py`以校正喔！
- 專案資料夾中，必須要有名為 `pet_photo` 的資料夾，才能夠儲存照片喔！
- 需要先申請 `imgur 的 clientID` 和 `Line Notify 的 token` 喔！

## DEMO 影片
1. [基本介紹](https://youtu.be/8-AfjUiVHws)
2. [飼料機安裝](https://youtu.be/fWW_FIBNFX0)
3. [倒飼料](https://youtu.be/yGEUHgmZ7uc)
4. [校正](https://youtu.be/oqYxFfcNnxE)
5. [寵物吃飯狀況回報](https://youtu.be/8sRyC3pKNv4)

## 檢討及可改進處
- 當初在準備材料時，沒有考慮到零件的固定方式，只能用熱溶膠和膠帶黏貼。如果能夠改用螺絲等鎖住會更好，也比較不會浪費膠。
- 原本的專題構想有回傳寵物吃飯速度這一資料，但是PIR sensor在判斷上還是有點太遲鈍，沒辦法很精準抓到寵物此時進來、寵物此時離開。
    - 因此改成計算一段固定時間(5分鐘)的質量變化，並回傳這筆資料給主人。
    - 如果改用影像辨識寵物此時在不在餵食範圍內，可能較容易達成這個功能。

## 參考與引用資料
1. [伺服馬達操作](https://tutorials-raspberrypi.com/raspberry-pi-servo-motor-control/)
2. [HX711 class引用](https://github.com/gandalf15/HX711)
3. [python 上傳imgur](https://ithelp.ithome.com.tw/articles/10241006)
4. [python 傳送Line Notify](https://ithelp.ithome.com.tw/articles/10234576)
