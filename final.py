import time
import RPi.GPIO as GPIO
import picamera
import pyimgur
import requests
import hx711_read_pet_food as hx_pet


# 定義各PIN腳
MONTOR_PIN = 18
LED_PIN = 23
DOUT_PIN = 6
PD_SCK_PIN = 5

GPIO.setmode(GPIO.BCM)
GPIO.setup(MONTOR_PIN, GPIO.IN)
GPIO.setup(LED_PIN, GPIO.OUT)
camera = picamera.PiCamera()


# upload imgur and send line notify
def setPath(time):
    PATH = "/[請改成樹莓派上的專案資料夾]/pet_photo/" + time + ".jpg" #請輸入樹莓派上專案資料夾
    return PATH

def setTitle(time):
    title = "Pet Feeder_" + time
    return title

def uploadImgur(time):
    CLIENT_ID = "YOUR_IMGUR_CLIENT_ID" # 請輸入你的Imgur Client ID
    im = pyimgur.Imgur(CLIENT_ID)

    uploaded_image = im.upload_image(setPath(time), title=setTitle(time))

    img_link = uploaded_image.link
    print(img_link)

    return img_link



# send line notify
def startNotify(format_time, img_link):
    message = format_time  + " 狗狗開始吃飯囉！"
    img = img_link
    lineNotifyMessage(message, img)
    
def endNotify(weight):
    message = "狗狗這次吃了 "+ str(round(weight, 2)) + " 公克！"
    lineNotifyMessage(message)

def lineNotifyMessage(msg, img=''):
    token = 'YOUR_LINE_NOTIFY_TOKEN' # 請輸入你的Line Notify token
    headers = {
        "Authorization": "Bearer " + token, 
        "Content-Type" : "application/x-www-form-urlencoded"
    }

    if img=='':
        payload = {
            'message': msg,
        }
    else:
        payload = {
            'message': msg,
            'imageThumbnail' : img, #imageThumbnail、imageFullsize為成對的圖片，各有尺寸大小
            'imageFullsize' : img,
        }

    r = requests.post("https://notify-api.line.me/api/notify", headers = headers, params = payload)
    return r.status_code


try:
    while True:
        # 若有反應亮led 方便偵錯
        GPIO.output(LED_PIN, GPIO.input(MONTOR_PIN))
        time.sleep(1)

        if GPIO.input(MONTOR_PIN) == 1 :
            #寵物開始吃飯時的質量
            weight0 = hx_pet.read_food(DOUT_PIN, PD_SCK_PIN)
            print("initial weight is: " + str(weight0))
            
            
            if weight0 == -100:
                print("initial the swap file first!")
                break
            
            # 拍照
            camera.vflip = True
            current_time = time.strftime('%Y%m%d_%H%M%S', time.localtime())
            format_time = time.strftime('%Y-%m-%d %H:%M', time.localtime())
            camera.capture( "pet_photo/"+current_time+".jpg" )

            # 上傳imgur
            img_link = uploadImgur(current_time)

            # 送出狗狗開始吃飯通知
            startNotify(format_time, img_link)
            
            # 以5分鐘作為間隔
            # 以30秒作為測試
            time.sleep(300)
            #time.sleep(30)

            # 計算5分鐘後吃的質量
            weight = weight0 - hx_pet.read_food(DOUT_PIN, PD_SCK_PIN)
            print("the weight is: "+ str(weight))            
            
            # 送出結算通知
            endNotify(weight)
            
except KeyboardInterrupt:
#except KeyboardInterrupt:
    print("stopped")
finally:
    GPIO.cleanup()
